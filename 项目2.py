import atexit
from concurrent.futures import thread
import tkinter as tk
from tkinter import ttk
import csv
from datetime import datetime
from tkinter.messagebox import showwarning
from win10toast import ToastNotifier

win = tk.Tk()
win.title("电池管理系统")
win.geometry("1200x500")

data = []

def toast():
  toaster = ToastNotifier()
  toaster.show_toast("有新设备要充电啦，快来看看！",threaded=True,duration=3)


#判断名称是否存在
def ifnameexist(name:str):
  for i in range(len(data)):
    if data[i][0]==name:
      showwarning(title = "警告",message = "该设备名称已存在!")
      return True
  return False

@atexit.register
def exit_callback():
  writeData(data)
  with open("time.txt", "w") as f:  # 打开文件
    f.write(str(datetime.now()))

#计时函数
def count():
  for i in range(len(data)):
    if int(data[i][5])-3>0:
      data[i][5]=str(int(data[i][5])-3)
    else :
      data[i][5]="0"
      toast()
    x=tview.get_children()
    print(x)
    i=0
    for item in x:
      if i+1<=len(data):
        tview.item(item,values=data[i])
      print(len(data))
      i+=1
  win.after(3000,count)
  
win.after(3000,count)

def writeData(d:list):
  with open('datafile.csv', 'w+',newline="") as f:
    writeDate = csv.writer(f)
    writeDate.writerows(d)
    f.close()



#读取数据文件
with open("datafile.csv", "r") as d:  #打开csv文件
    readData = csv.reader(d)  #读取csv文件
    for row in readData:
        data.append(row)  #读入
d.close()
print(data)

#获取上次时间
lasttime = ""
with open("time.txt", "r+") as f:  # 打开文件
    lasttime = f.read()
    f.close()  # 读取时间字符串

#时间同步
datetime_nowtime=datetime.now()
datetime_lasttime=datetime.strptime(lasttime,"%Y-%m-%d %H:%M:%S.%f")
delta_time=(datetime_nowtime-datetime_lasttime).seconds
box=0
for i in range(len(data)):
  if int(data[i][5])> delta_time:
    box=int(data[i][5])
    box-=delta_time
    data[i][5]=str(box)
  else :
    data[i][5]="0"

#以下是初始化界面
#初始化列表视图
tview = ttk.Treeview(
    win,
    show="headings",
    columns=["name", "type", "statu", "batterymax", "batterytype", "time"])
tview.heading("name", text="设备名称")
tview.heading("type", text="设备类型")
tview.heading("statu", text="设备状态")
tview.heading("batterymax", text="电池容量")
tview.heading("batterytype", text="电池种类")
tview.heading("time", text="建议充电时间/s")
tview.pack()
#初始化输入框
frame_name=tk.Frame(win)
frame_name.pack()
t_name=tk.Label(frame_name,text="设备名称")
t_name.grid(row=1,column=1)
name_entry = tk.Entry(frame_name)
name_entry.grid(row=1,column=2)

frame_type=tk.Frame(win)
frame_type.pack()
t_type=tk.Label(frame_type,text="设备类型")
t_type.grid(row=1,column=1)
type_entry = tk.Entry(frame_type)
type_entry.grid(row=1,column=2)

frame_statu=tk.Frame(win)
frame_statu.pack()
t_statu=tk.Label(frame_statu,text="设备状态")
t_statu.grid(row=1,column=1)
statu_entry = tk.Entry(frame_statu)
statu_entry.grid(row=1,column=2)

frame_batmax=tk.Frame(win)
frame_batmax.pack()
t_batmax=tk.Label(frame_batmax,text="电池容量")
t_batmax.grid(row=1,column=1)
batterymax_entry = tk.Entry(frame_batmax)
batterymax_entry.grid(row=1,column=2)

frame_battype=tk.Frame(win)
frame_battype.pack()
t_battype=tk.Label(frame_battype,text="电池类型")
t_battype.grid(row=1,column=1)
batterytype_entry = tk.Entry(frame_battype)
batterytype_entry.grid(row=1,column=2)


#增删改函数
def add():
    i = len(data) + 1
    name = name_entry.get()
    type_ = type_entry.get()
    statu = statu_entry.get()
    batterymax = batterymax_entry.get()
    batterytype = batterytype_entry.get()
    time = 0
    if name!='' and type_!='' and statu!='' and batterymax!='' and batterytype!='' and ifnameexist(name)==False:
      if "锂" in batterytype:
       if "关" in statu or "不" in statu or "停" in statu:
        time=2678400*2
       elif "开" in statu or "正" in statu or "中" in statu:
        time=3600*8
       else:
        time=3600*24
      elif "镍氢" in batterytype:
       if "关" in statu or "不" in statu or "停" in statu:
        time=31536000*2
       elif "开" in statu or "正" in statu or "中" in statu:
        time=2678400*2
       else:
        time=2678400*1
      elif "铅酸" in batterytype:
       if "关" in statu or "不" in statu or "停" in statu:
        time=2678400*3
       elif "开" in statu or "正" in statu or "中" in statu:
        time=3600*8
       else:
        time=3600*8
      else:
        time=3600*24
      data.append([name, type_, statu, batterymax, batterytype, time])
      writeData(data)
      tview.insert('',i,values=[name, type_, statu, batterymax, batterytype, time])

def delete():
    item_value=tview.item(tview.selection(),'values')
    for i in range(len(data)):
      if data[i][0]==item_value[0]:
        data.pop(i)
        break
    tview.delete(tview.selection())
    writeData(data)

def edit():
    name = name_entry.get()
    type_ = type_entry.get()
    statu = statu_entry.get()
    batterymax = batterymax_entry.get()
    batterytype = batterytype_entry.get()
    item_value=tview.item(tview.selection(),'values')
    for i in range(len(data)):
      if data[i][0]==item_value[0] and ifnameexist(name)==False:
        if name!='':
          data[i][0]=name
        if type_!='':
          data[i][1]=type_
        if statu!='':
          data[i][2]=statu
        if batterymax!='':
          data[i][3]=batterymax
        if batterytype!='':
          data[i][4]=batterytype
        tview.item(tview.selection(),
               values=data[i])
        writeData(data)


#初始化按钮

#初始化按钮
add_but = tk.Button(text="add", command=add)
add_but.pack()
del_but = tk.Button(text="delete", command=delete)
del_but.pack()
edit_but = tk.Button(text="edit", command=edit)
edit_but.pack()
#向列表视图中载入数据
for i, item in enumerate(data):
    tview.insert('', i, values=item)


win.mainloop()